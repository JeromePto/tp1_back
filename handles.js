const url = require("url");
const qs = require("querystring");

const serverHandle = (req, res) => {
    const route = url.parse(req.url);
    const path = route.pathname;
    const params = qs.parse(route.query);

    

    if (path === "/hello" && params["name"]) {
        res.writeHead(200, { "Content-Type": "text/plain" });
        if (params["name"] === "jerome") {
            res.write("My short intro");
        } else {
            res.write("Hello " + params["name"]);
        }
    } else {
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.write("404 Error");
    }
    res.end();
};

module.exports = {
    serverHandle,
}